﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {


    [System.Serializable]
    public class Stats
    {
        public float speed;
        public float rotSpeed;
        public float bulletDamage;
        public float splashDamage;
        public float exploRadius;

        public int ricochets;
        public int chains;
    }

    public Turret.Stats stats;

    //public float speed = 2f;
    //public float rotSpeed = 50f;
    [HideInInspector]
    public Transform target;

    [HideInInspector]
    public Transform parentTurret;
    [HideInInspector]
    public float parentTurretRange;


    [HideInInspector]
    public Vector3 lastKnownPosition;

    [HideInInspector]
    public bool isTargetAlive = false;



	// Use this for initialization
	void Start () {

        SetRotationToTarget();
        isTargetAlive = true;
    }
	
	// Update is called once per frame
	void Update () {

        CheckIfTargetIsAlive();
        MoveTowardTheTarget();
        SetRotationToTarget();

    }
    void MoveTowardTheTarget()
    {
        Vector2 dir;
        if (isTargetAlive)
        {
            dir = target.position - this.transform.localPosition;
        }
        else
        {
            dir = lastKnownPosition - this.transform.localPosition;
        }
        float distThisFrame = stats.speed * Time.deltaTime;

        if (dir.magnitude <= distThisFrame)
        {
            BulletHit();
        }
        
        transform.Translate(dir.normalized * distThisFrame, Space.World);
        Quaternion targetRotation = Quaternion.LookRotation(dir);
        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, Time.deltaTime * 5);
    }
    public void SetRotationToTarget()
    {
        Vector3 vectorToTarget;
        if (isTargetAlive)
        {
            vectorToTarget = target.position - transform.position;
        }
        else
        {
            vectorToTarget = lastKnownPosition - transform.position;
        }

        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        transform.rotation = Quaternion.Lerp(transform.rotation, q, Time.deltaTime * stats.rotSpeed);
    }

    #region Bullet Hit Target

    virtual public void BulletHit()
    {
        if(target != null)
        {
            Enemy enemy = target.GetComponent<Enemy>();
            enemy.TakeDamage(stats.bulletDamage);
            if(enemy != null)
            {
                enemy.TakePoisonDamage(stats.poisonDamage);
                enemy.GetSlowed(stats.slow, stats.slowTime);
                enemy.GetStunned(stats.stun, stats.stunTime);
            }
        }
        if(stats.ricochets > 0)
        {
            LookForNewTarget();
            stats.ricochets--;
        }
        if(stats.splashRadius > 0)
        {
            Collider[] colls = Physics.OverlapSphere(transform.position, stats.splashRadius);
            foreach (Collider c in colls)
            {
                Enemy e = c.GetComponent<Enemy>();
                float d = Vector3.Distance(this.transform.position, e.transform.position);
                float ratio = (1 - d / stats.splashRadius) + 0.5f;
                if (e != null)
                {
                    e.TakeSplashDamage(stats.splashDamage * ratio);
                    if (e != null)
                    {
                        e.TakePoisonDamage(stats.poisonDamage * ratio);
                        e.GetSlowed(stats.slow * ratio, stats.slowTime);
                        e.GetStunned(stats.stun * ratio, stats.stunTime);
                    }
                }
            }
        }
        if(stats.ricochets<=0 || target == null)  Destroy(gameObject);
    }

    void LookForNewTarget()
    {
        Collider[] colls = Physics.OverlapSphere(transform.position, 1);
        if(colls.Length == 1 && target != null && colls[0].GetComponent<Enemy>().transform == target.transform)
        {
            stats.ricochets = 0;
        }
        if (colls.Length > 0)
        {
            float dist = Mathf.Infinity;
            foreach (Collider c in colls)
            {
                Enemy e = c.GetComponent<Enemy>();
                if (e != null)
                {
                    if (target != null && target.transform == e.transform)
                    {
                        continue;
                    }
                    float d = Vector3.Distance(transform.position, e.transform.position);
                    if (d < dist)
                    {
                        dist = d;

                        target = e.transform;
                    }
                }
            }
        }
    }


    #endregion

    void CheckIfTargetIsAlive()
    {
        if(target != null)
        {
            lastKnownPosition = target.position;
        }
        else
        {
            isTargetAlive = false;
        }
    }

    public void AssignStats(Turret.Stats new_stats)
    {

        /*
        this.stats.bulletDamage = stats.bulletDamage;
        this.stats.splashDamage = stats.splashDamage;
        this.stats.exploRadius = stats.exploRadius;
        this.stats.speed = stats.speed;
        */

        this.stats = (Turret.Stats) new_stats.Clone();


     
    }

}
