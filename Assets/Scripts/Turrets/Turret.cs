﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Turret : MonoBehaviour {

    [System.Serializable]
    public class Stats : System.ICloneable
    {
        public bool doesTremor;
        public float tremorCooldown;
        public float tremorRadius;
        public float tremorDamage;


        public float fireCooldown;
        public float turretRange;
        public float bulletDamage;

        public float speed;
        public float rotSpeed;

        public float splashRadius;
        public float splashDamage;
        public float poisonDamage;

        public float criticalStrikeChance;
        public float criticalStrikeDamage;

        public float slow;
        public float slowTime;

        public float stun;
        public float stunTime;

        public int ricochets;
        public float ricochetRange;
        public bool isChaining = false;
        public float chainRange;

        public object Clone()
        {
            return this.MemberwiseClone();
        }

    }
    public class ResValues
    {
        public int materia;
        public int redEssence;
        public int greenEssence;
        public int blueEssence;
        public int cyanEssence;
        public int yellowEssence;
        public int violetEssence;
        public int greyEssence;
        public int blackEssence;

        public ResValues() { }
        public ResValues(int _materia, int _r, int _g, int _b, int _c, int _y, int _v, int _grey, int _black)
        {
            materia = _materia;
            redEssence = _r;
            greenEssence = _g;
            blueEssence = _b;
            cyanEssence = _c;
            yellowEssence = _y;
            violetEssence = _v;
            greyEssence = _grey;
            blackEssence = _black;
        }
        public void IncreaseCost(int amount, EssenceColour colour)
        {
            switch (colour)
            {

                case EssenceColour.NEUTRAL:
                    materia += amount;
                    break;
                case EssenceColour.RED:
                    redEssence += amount;
                    break;
                case EssenceColour.GREEN:
                    greenEssence += amount;
                    break;
                case EssenceColour.BLUE:
                    blueEssence += amount;
                    break;
                case EssenceColour.CYAN:
                    cyanEssence += amount;
                    break;
                case EssenceColour.YELLOW:
                    yellowEssence += amount;
                    break;
                case EssenceColour.VIOLET:
                    violetEssence += amount;
                    break;
                case EssenceColour.GREY:
                    greyEssence += amount;
                    break;
                case EssenceColour.BLACK:
                    blackEssence += amount;
                    break;
            }
        }
    }
    [System.Serializable]
    public struct Upgrades
    {
        public byte level;
        public byte redTier;
        public byte greenTier;
        public byte blueTier;
        public byte cyanTier;
        public byte yellowTier;
        public byte violetTier;
        public byte greyTier;
        public byte blackTier;
        public void UpgradeTotalLevel() { level++; }
        public void ResetTiers()
        {
            redTier = 0;
            greenTier = 0;
            blueTier = 0;
            cyanTier = 0;
            yellowTier = 0;
            violetTier = 0;
            greyTier = 0;
            blackTier = 0;
    }
        public void UpgradeTier(EssenceColour colour)
        {
            switch (colour)
            {
                case EssenceColour.RED:
                    redTier++;
                    break;
                case EssenceColour.GREEN:
                    greenTier++;
                    break;
                case EssenceColour.BLUE:
                    blueTier++;
                    break;
                case EssenceColour.CYAN:
                    cyanTier++;
                    break;
                case EssenceColour.YELLOW:
                    yellowTier++;
                    break;
                case EssenceColour.VIOLET:
                    violetTier++;
                    break;
                case EssenceColour.GREY:
                    greyTier++;
                    break;
                case EssenceColour.BLACK:
                    blackTier++;
                    break;
            }
        }
    }
    [System.Serializable]
    public class UpgradeCosts
    {
        public int nextLevelCost;
        public int tierEssenceCost;
        public bool isUnlocked;
        public bool isLast;

    }

    public string turretName;
    public Stats stats;
    public ResValues totalCost = new ResValues();
    public ResValues essenceCosts = new ResValues(0, 1, 1, 1, 1, 1, 1, 1, 1);
    public List<UpgradeCosts> upgradeInformations;



    byte tiersLeft = 5;
    byte maxTiersPerUpgrade = 5;

    float fireCooldownLeft = 0f;
    float tremorCooldownLeft = 0f;


    public GameObject BulletPrefab;
    public GameObject turretRangeCircle;
    public GameObject tremorRangeCircle;

    
    
    public Transform cannonTip;
    public Transform turretBase;
    public Transform rangecircle;
    public Transform tremorcircle;
    
    //[HideInInspector]
    public Transform turretCannonTransform;
    [HideInInspector]
    public Enemy target = null;

    public Upgrades totalLevels;
    public Upgrades currentLevels;


    public Stats GetTurretStats()
    {
        return stats;
    }
    public string GetTurretName()
    {
        return name;
    }
    
    public Upgrades GetCurrentTurretUpgrades()
    {
        return currentLevels;
    }
    public Upgrades GetTotalTurretUpgrades()
    {
        return totalLevels;
    }
    
    public int GetEssenceCosts(EssenceColour colour)
    {
        int essence_cost;

        switch (colour)
        {
            case EssenceColour.RED:
                essence_cost = (currentLevels.redTier + 1) * upgradeInformations[totalLevels.level - 1].tierEssenceCost;
                return essence_cost;
            case EssenceColour.GREEN:
                essence_cost = (currentLevels.greenTier + 1) * upgradeInformations[totalLevels.level - 1].tierEssenceCost;
                return essence_cost;
            case EssenceColour.BLUE:
                essence_cost = (currentLevels.blueTier + 1) * upgradeInformations[totalLevels.level - 1].tierEssenceCost;
                return essence_cost;
            case EssenceColour.CYAN:
                essence_cost = (currentLevels.cyanTier + 1) * upgradeInformations[totalLevels.level - 1].tierEssenceCost;
                return essence_cost;
            case EssenceColour.YELLOW:
                essence_cost = (currentLevels.yellowTier + 1) * upgradeInformations[totalLevels.level - 1].tierEssenceCost;
                return essence_cost;
            case EssenceColour.VIOLET:
                essence_cost = (currentLevels.violetTier + 1) * upgradeInformations[totalLevels.level - 1].tierEssenceCost;
                return essence_cost;
            case EssenceColour.GREY:
                essence_cost = (currentLevels.greyTier + 1) * upgradeInformations[totalLevels.level - 1].tierEssenceCost;
                return essence_cost;
            case EssenceColour.BLACK:
                essence_cost = (currentLevels.blackTier + 1) * upgradeInformations[totalLevels.level - 1].tierEssenceCost;
                return essence_cost;
        }
        return 0;
    }

    // Use this for initialization
    void Start ()
    {
        totalLevels.level = 1;
        currentLevels.level = 1;

        if(turretRangeCircle != null)
        {
            rangecircle.localScale = new Vector3(2 * stats.turretRange, 2 * stats.turretRange, 1);
            turretRangeCircle.SetActive(false);
        }
        if(tremorRangeCircle != null)
        {
            tremorcircle.localScale = new Vector3(2 * stats.tremorRadius, 2 * stats.tremorRadius, 1);
            tremorRangeCircle.SetActive(false);
        }
        turretCannonTransform = transform.Find("TurretCannon");
        if (turretCannonTransform != null)
        {
            turretCannonTransform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
        }
        

    }
	
	// Update is called once per frame
	void Update ()
    {
        CheckNeedForNewTarget();
        //Shoot da bullet!
        fireCooldownLeft -= Time.deltaTime;
        tremorCooldownLeft -= Time.deltaTime;

        if (target != null && turretCannonTransform != null) 
        {
            float dir = Vector3.Distance(turretBase.transform.position, target.transform.position);

            if (turretCannonTransform != null) RotateCannonAtTarget();
            if (fireCooldownLeft <= 0 && dir <= stats.turretRange)
            {
                fireCooldownLeft = stats.fireCooldown;
                ShootAt();
            }
        }
        if (stats.doesTremor && tremorCooldownLeft <= 0)
        {
            Collider[] colls = Physics.OverlapSphere(transform.position, stats.tremorRadius);
            if(colls.Length > 0)
            {
                MakeTremor(colls);
                tremorCooldownLeft = stats.tremorCooldown;
            }
        }
	}


    void OnMouseUp()
    {
        TurretBuilder TB = GameObject.FindObjectOfType<TurretBuilder>();
        if (TB.isBuilding()) return;
        EntitySelector selector = FindObjectOfType<EntitySelector>();
        selector.SelectTurret(this);
    }

    public void SelectTurret(bool isSelected)
    {
        if (turretRangeCircle != null) turretRangeCircle.SetActive(isSelected);
        if (tremorRangeCircle != null) tremorRangeCircle.SetActive(isSelected);
    }


    virtual public void ShootAt()
    {
        GameObject bulletGO = (GameObject)Instantiate(BulletPrefab, cannonTip.position, cannonTip.rotation);
        Bullet b = bulletGO.GetComponent<Bullet>();
        b.target = target.transform;
        b.AssignStats(stats);
        b.parentTurret = this.transform;
        b.parentTurretRange = this.stats.turretRange;
        //b.SetRotationToTarget();
    }

    public void MakeTremor(Collider[] colls)
    {
        foreach (Collider c in colls)
        {
            Enemy e = c.GetComponent<Enemy>();
            if (e != null)
            {
                e.TakeSplashDamage(stats.tremorDamage);
                e.TakePoisonDamage(stats.poisonDamage);
                e.GetSlowed(stats.slow, stats.slowTime);
                e.GetStunned(stats.stun, stats.stunTime);
            }
        }
    }

    virtual public void RotateCannonAtTarget()
    {
        Vector3 vectorToTarget = target.transform.position - turretCannonTransform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        turretCannonTransform.rotation = Quaternion.Euler(0, 0, q.eulerAngles.z + 90);
    }

    void CheckNeedForNewTarget()
    {
        if (target == null)
        {
            TargetNewEnemy();
        }
        else
        {
            float dir = Vector3.Distance(turretBase.transform.position, target.transform.position);
            if (dir > stats.turretRange)
            {
                target = null;
            }
        }
    }

    void TargetNewEnemy()
    {
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        float dist = Mathf.Infinity;
        //Debug.Log(enemies.Length);
        foreach(Enemy e in enemies)
        {
            float d = Vector3.Distance(turretBase.transform.position, e.transform.position);
            if (d > stats.turretRange)
            {
                continue;
            }
            if (target == null || d < dist)
            {
                target = e;
                dist = d;
            }
        }
        if(target != null)
            RotateCannonAtTarget();
        
    }


    #region Turret_upgrades

    public bool CanUpgradeTurret()
    {
        GameManager gm = GameObject.FindObjectOfType<GameManager>();

        if (upgradeInformations[totalLevels.level - 1].isLast) return false;

        if (upgradeInformations[totalLevels.level].isUnlocked && gm.GetMateriaAmount() >= upgradeInformations[totalLevels.level].nextLevelCost)
        {
            return true;
        }
        return false;
    }

    public bool CanIncreaseTurretTier(EssenceColour colour)
    {
        if (tiersLeft == 0) return false;

        GameManager gm = GameObject.FindObjectOfType<GameManager>();
        //int essence_cost;

        switch (colour)
        {
            case EssenceColour.RED:
                if (gm.GetEssenceAmount().redEssence >= GetEssenceCosts(colour))
                    return true;
                break;
            case EssenceColour.GREEN:
                if (gm.GetEssenceAmount().greenEssence >= GetEssenceCosts(colour))
                    return true;
                break;
            case EssenceColour.BLUE:
                if (gm.GetEssenceAmount().blueEssence >= GetEssenceCosts(colour))
                    return true;
                break;
            case EssenceColour.CYAN:
                if (gm.GetEssenceAmount().cyanEssence >= GetEssenceCosts(colour))
                    return true;
                break;
            case EssenceColour.YELLOW:
                if (gm.GetEssenceAmount().yellowEssence >= GetEssenceCosts(colour))
                    return true;
                break;
            case EssenceColour.VIOLET:
                if (gm.GetEssenceAmount().violetEssence >= GetEssenceCosts(colour))
                    return true;
                break;
            case EssenceColour.GREY:
                if (gm.GetEssenceAmount().greyEssence >= GetEssenceCosts(colour))
                    return true;
                break;
            case EssenceColour.BLACK:
                if (gm.GetEssenceAmount().blackEssence >= GetEssenceCosts(colour))
                    return true;
                break;

            default: return false;
        }



        return false;
    }

    public void UpgradeTurret()
    {
        totalCost.materia += upgradeInformations[totalLevels.level].nextLevelCost;

        tiersLeft = maxTiersPerUpgrade;
        totalLevels.level++;
        currentLevels.level++;
        currentLevels.ResetTiers();

        UpgradeTurretStats();
    }

    virtual public void UpgradeTurretStats()
    {
        stats.turretRange *= 1.3f;
        stats.fireCooldown *= 0.85f;
        rangecircle.localScale = new Vector3(2 * stats.turretRange, 2 * stats.turretRange, 1);
    }


    public void IncreaseTurretTier(EssenceColour colour)
    {
        tiersLeft--;
        int cost = GetEssenceCosts(colour);
        totalCost.IncreaseCost(cost, colour);

        totalLevels.UpgradeTier(colour);
        currentLevels.UpgradeTier(colour);

        IncreaseTurretTierStats(colour);
        
    }

    virtual public void IncreaseTurretTierStats(EssenceColour colour)
    {
        switch (colour)
        {
            case EssenceColour.RED:
                stats.bulletDamage *= 1.1f;
                break;

            default: return;
        }
    }

    #endregion
    public void IncreaseTurretValue(int value)
    {
        totalCost.materia += value;
    }


    public void SellTurret()
    {
        GameManager gm = GameObject.FindObjectOfType<GameManager>();

        gm.ResourceIncome(totalCost.materia, EssenceColour.NEUTRAL);
        gm.ResourceIncome(totalCost.redEssence, EssenceColour.RED);
        gm.ResourceIncome(totalCost.greenEssence, EssenceColour.GREEN);
        gm.ResourceIncome(totalCost.blueEssence, EssenceColour.BLUE);
        gm.ResourceIncome(totalCost.cyanEssence, EssenceColour.CYAN);
        gm.ResourceIncome(totalCost.yellowEssence, EssenceColour.YELLOW);
        gm.ResourceIncome(totalCost.violetEssence, EssenceColour.VIOLET);
        gm.ResourceIncome(totalCost.greyEssence, EssenceColour.GREY);
        gm.ResourceIncome(totalCost.blackEssence, EssenceColour.BLACK);

        Destroy(gameObject);

    }

}
