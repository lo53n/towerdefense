﻿using UnityEngine;
using System.Collections;

public static class Land {

    public struct UpgradeEffectCheck
    {
        public bool damage;
        public bool bulletDamage;
        public bool battleIncome;
        public bool afterBattleIncome;
        public bool range;
        public bool rateOfFire;
        public bool critChance;
        public bool critMultiplier;
        public bool stunChance;
        public bool stunDuration;
        public bool slowDuration;
        public bool splashRadius;
        public bool armourPenetration;
        public bool chaining;
    };
    public struct UpgradeEffectValue
    {
        public float damage;
        public float bulletDamage;
        public float battleIncome;
        public float afterBattleIncome;
        public float range;
        public float rateOfFire;
        public float critChance;
        public float critMultiplier;
        public float stunChance;
        public float stunDuration;
        public float slowDuration;
        public float splashRadius;
        public float armourPenetration;
        public int chaining;
    };


    public struct UpgradeData
    {
        public int id;
        public int currentLevel;
        public int maxLevel;
        public UpgradeEffectCheck checks;
        public UpgradeEffectValue values;
        public int baseCost;
        public int currentCost;
        public bool isExponential;
        public int costIncreasePerLevel;
        public float costIncreasePerLevelPercentage;

    };

    public struct Tier1Upgrades
    {
        public UpgradeData damageTech;
        public UpgradeData afterBattleIncomeTech;

        public UpgradeData nextTierTech;
    };

    public struct Tier2Upgrades
    {

    };

    public struct Tier3Upgrades
    {

    };

    public struct Tier4Upgrades
    {

    };

    public struct Tier5Upgrades
    {

    };

    public struct Tier6Upgrades
    {

    };

    public static Tier1Upgrades t1;
    public static bool t2unlocked = false;
    public static Tier2Upgrades t2;
    public static bool t3unlocked = false;
    public static Tier3Upgrades t3;
    public static bool t4unlocked = false;
    public static Tier4Upgrades t4;
    public static bool t5unlocked = false;
    public static Tier5Upgrades t5;
    public static bool t6unlocked = false;
    public static Tier6Upgrades t6;
    

    public static void LoadUpgradesData()
    {
        LoadT1UpgradesData();
    }


    public static void LoadT1UpgradesData()
    {
        /* t1.damageTech = new UpgradeData() { id = 0,
                                             baseCost = 500,
                                             currentCost = baseCost,
                                             isExponential = true,
                                             costIncreasePerLevelPercentage = 1.20};*/
        t1.damageTech = new UpgradeData() { id = 0,
                                            baseCost = 500,
                                            currentCost = 500,
                                            isExponential = true,
                                            costIncreasePerLevelPercentage = 1.20f,
                                            checks = new UpgradeEffectCheck() {damage = true },
                                            values = new UpgradeEffectValue() {damage = 0.05f } };



    }


}
