﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

    [System.Serializable]
    public class Wave
    {
        public float waveTimer;
        public WaveComposition[] comp;
    } 

    [System.Serializable]
    public class WaveComposition
    {
        public GameObject enemyPrefab;
        public float spawnCooldown;
        public int size;
        [System.NonSerialized]
        public int spawned = 0;
    }

    
    public Wave[] wave;
    int currentWave = 0;
    int currentWaveComposition;

    [HideInInspector]
    public int waveNumber;
    [HideInInspector]
    public int maxWave;



    float waveTimerLeft = 0;
    bool sentEarly = false;
    float cooldownLeft = 0;
    bool nextWave = true;

	// Use this for initialization
	void Start () {
        maxWave = wave.Length;
        cooldownLeft = wave[0].comp[0].spawnCooldown;
	}
	
	// Update is called once per frame
	void Update () {
        waveTimerLeft -= Time.deltaTime;
        cooldownLeft -= Time.deltaTime;
        if(cooldownLeft <= 0 && waveTimerLeft <= 0 && nextWave)
        {
            bool didSpawn = false;

            foreach(WaveComposition wc in wave[currentWave].comp)
            {
                if (wc.spawned < wc.size)
                {
                    cooldownLeft = wc.spawnCooldown;
                    wc.spawned++;

                    GameObject enemy = (GameObject) Instantiate(wc.enemyPrefab, this.transform.position, this.transform.rotation);
                    enemy.GetComponent<Enemy>().setPath(transform.GetChild(0).gameObject);
                    didSpawn = true;
                    break;
                }
            }

            if (!didSpawn)
            {
                currentWave++;
                if (currentWave == maxWave) nextWave = false;
                else waveTimerLeft = wave[currentWave].waveTimer;

                if (sentEarly)
                {
                    waveTimerLeft = 0;
                    sentEarly = false;
                }

            }

        }

	}

    public void SendWaveEarlier()
    {
        sentEarly = true;
        waveTimerLeft = 0;
        Debug.Log("Sent");
        //TODO:
        //Add some resource bonus?
    }
}
