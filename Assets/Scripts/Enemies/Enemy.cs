﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{

    [System.Serializable]
    public class Stats
    {
        public int damage;
        public float speed;
        public float originalSpeed;
        public float health;
        public float maxHealth;
        public float armour;
        public float damageReduction;
        public float splashResistance;
        public float zapResistance;
        public float stunResistance;
        public float slowResistance;
        public float poisonResistance;

        public EssenceColour essenceColour;
        public int materiaReward;
        public int essenceReward;

    }

    bool isPoisoned;
    public float totalPoisonBuildup = 0f;
    public float poisonBuildup = 0f;
    float poisonTickTime = 0.25f;
    float poisonTickTimeRemaining = 0f;

    bool isSlowed;
    float slowPercentage;
    float slowTimeRemaining;
    float slowWearOffRatio;

    bool isStunned;
    float stunTimeRemaining;
    int closeStuns = 0;
    float perCloseStunIncrease = 0.75f;

    GameObject pathGO;

    public Transform healthBar;
    Transform targetPathNode;
    int pathNodeIndex = 0;


    public Stats stats;

    // Use this for initialization
    void Start()
    {
        //GetComponent<Rigidbody>().velocity = new Vector2(-speed, 0);
        //pathGO = GameObject.Find("Enemy Path");

        healthBar.GetChild(1).GetComponent<SpriteRenderer>().color = new Color(0, 1, 0);
        healthBar.position = new Vector3(transform.position.x, transform.position.y + 0.25f, transform.position.z);

        stats.maxHealth = stats.health;
    }

    public void setPath(GameObject newPathGO)
    {
        pathGO = newPathGO;
    }
        void GetNextPathNode()
    {
        if (pathNodeIndex < pathGO.transform.childCount)
        {
            targetPathNode = pathGO.transform.GetChild(pathNodeIndex);
            pathNodeIndex++;
        }
        else
        {
            pathNodeIndex = 0;
            ReachGoal();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //First, wear of statuses
        if (isPoisoned) WearOffPoison();
        if (isSlowed) WearOffSlow();

        if(isStunned == true)
        {
            WearOffStun();
            return;
        }


        if (targetPathNode == null)
        {
            GetNextPathNode();
            if (targetPathNode == null)
            {
                //ReachGoal();
                return;
            }
        }

        Vector3 dir = targetPathNode.position - this.transform.localPosition;
        float distThisFrame = stats.speed * Time.deltaTime;

        if (dir.magnitude <= distThisFrame)
        {
            targetPathNode = null;
        }
        else
        {
            transform.Translate(dir.normalized * distThisFrame, Space.World);


            Vector3 vectorToTarget = targetPathNode.transform.position - this.transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);
            this.transform.rotation = Quaternion.Euler(0, 0, q.eulerAngles.z + 90);
            healthBar.rotation = Quaternion.Euler(0, 0, 0);
            healthBar.position = new Vector3(transform.position.x, transform.position.y + 0.25f, transform.position.z);

        }

    }

    public void RefreshHealthBar()
    {
        healthBar.GetChild(1).GetComponent<SpriteRenderer>().color = new Color(1 - (stats.health / stats.maxHealth), stats.health / stats.maxHealth, 0);
        healthBar.transform.localScale = new Vector3((stats.health / stats.maxHealth), 1, 1);
    }

    #region Enemy takes direct hit/aoe damage

    public void TakeDamage(float damage)
    {
        float actualDamage = (damage * (1f - stats.damageReduction)) - stats.armour;
        if (actualDamage > 0)
        {
            stats.health -= actualDamage;
        }
        CheckHealth();
    }
    public void TakeSplashDamage(float damage)
    {
        float actualDamage = (damage * (1f - stats.splashResistance)) - stats.armour;
        //Debug.Log("Took " + actualDamage + " dmg");
        if (actualDamage > 0)
        {
            stats.health -= actualDamage;
        }
        CheckHealth();
    }
    public void TakeZapDamage(float damage)
    {
        float actualDamage = damage * (1f - stats.zapResistance);
        if (actualDamage > 0)
        {
            stats.health -= actualDamage;
        }
        CheckHealth();
    }

    public void TakePoisonDamage(float damage)
    {
        float actualDamage = damage * (1f - stats.poisonResistance);
        //Debug.Log(actualDamage);
        totalPoisonBuildup += actualDamage;
        poisonBuildup += actualDamage;


    }


    #endregion
    #region Enemy Status

    void WearOffPoison()
    {
        if (totalPoisonBuildup > 0)
        {
            poisonTickTimeRemaining -= Time.deltaTime;
            if (poisonTickTimeRemaining <= 0)
            {
                float dmg = totalPoisonBuildup * 0.1f;
                if (dmg > poisonBuildup)
                {
                    stats.health -= poisonBuildup;
                    totalPoisonBuildup = 0;
                    poisonBuildup = 0;
                    isPoisoned = false;
                }
                else
                {
                    stats.health -= dmg;
                    poisonBuildup -= dmg;
                }
                CheckHealth();
                poisonTickTimeRemaining = poisonTickTime;
            }
        }
    }
    public void GetSlowed(float slowValue, float slowTime)
    {
        if (slowValue >= 1) slowValue = 0.99f;
        if (slowValue <= 0 || slowTime <= 0) return;
        slowValue = slowValue * (1 - stats.slowResistance);
        slowTime = slowTime * (1 - stats.slowResistance);
        if (slowValue > slowPercentage)
        {
            slowPercentage = slowValue;
        }
        if (slowTime > slowTimeRemaining)
        {
            slowTimeRemaining = slowTime;
        }
        isSlowed = true;
        slowWearOffRatio = slowPercentage / slowTime;
        stats.speed = stats.originalSpeed * (1 - slowPercentage);
    }
    void WearOffSlow()
    {
        slowTimeRemaining -= Time.deltaTime;
        slowPercentage -= slowWearOffRatio * Time.deltaTime;
        stats.speed = stats.originalSpeed * (1 - slowPercentage);
        if(slowTimeRemaining <= 0 || slowPercentage <= 0)
        {
            isSlowed = false;
            slowPercentage = 0f;
            slowTimeRemaining = 0f;
            stats.speed = stats.originalSpeed;

        }
    }
    public void GetStunned(float stunChance,float stunTime)
    {
        if (stunChance <= 0 || stunTime <= 0) return;
        float extraChance = Mathf.Pow(closeStuns, perCloseStunIncrease);
        stunChance = (stunChance + extraChance) * (1 - stats.stunResistance);
        stunTime = stunTime * (1 - stats.stunResistance);

        float chance = Random.Range(0f, 1f);
        if(chance <= stunChance)
        {
            isStunned = true;
            stunTimeRemaining = stunTime;
            closeStuns = 0;
        }
        else
        {
            if(2 * chance <= stunChance)
            {
                closeStuns++;
            }
        }
    }
    void WearOffStun()
    {
        stunTimeRemaining -= Time.deltaTime;
        if(stunTimeRemaining <= 0f)
        {
            isStunned = false;
        }
    }

    #endregion
    #region Enemy life and death matters

    void CheckHealth()
    {
        //Debug.Log(health);
        if (stats.health <= 0)
        {
            KillEnemy();
        }
        else
        {
            RefreshHealthBar();
        }
    }

    public void KillEnemy()
    {
        GameManager gm = GameObject.Find("__GAME_SCRIPTS__").GetComponent<GameManager>();

        gm.ResourceIncome(stats.materiaReward, EssenceColour.NEUTRAL);
        gm.ResourceIncome(stats.essenceReward, stats.essenceColour);

        Destroy(gameObject);
    }

    #endregion
    void ReachGoal()
    {
        Debug.Log("Reached goal");
        GameObject.Find("__GAME_SCRIPTS__").GetComponent<GameManager>().TakeLife(stats.damage);
        Destroy(gameObject);

    }

}
