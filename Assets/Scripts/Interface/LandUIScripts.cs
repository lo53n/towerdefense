﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class LandUIScripts : MonoBehaviour{

    public void ShowMapList()
    {
        GameObject.Find("Main Interface").transform.Find("Map list").gameObject.SetActive(true);
    }

    public void HideMapList()
    {
        GameObject.Find("Main Interface").transform.Find("Map list").gameObject.SetActive(false);
    }

    public void SelectGameMap()
    {
        int mapNumber = FindObjectOfType<EventSystem>().currentSelectedGameObject.transform.GetSiblingIndex();
        GlobalsAndEnums.currentMap = mapNumber;
    }
}
