﻿using UnityEngine;
using System.Collections;

public class TurretPlacement : MonoBehaviour
{

    int triggers = 0;

    // Use this for initialization
    void Start()
    {
        setAsPermit();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        triggers++;
        setAsDenial();
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        triggers--;
        if(triggers == 0) setAsPermit();
    }

    void setAsPermit()
    {
        gameObject.GetComponent<SpriteRenderer>().color = new Color(0, 1, 0, 0.3f);
        GameObject.FindObjectOfType<TurretBuilder>().setBuildingPermit(true);
    }
    void setAsDenial()
    {
        gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 0, 0, 0.3f);
        GameObject.FindObjectOfType<TurretBuilder>().setBuildingPermit(false);
    }
}
