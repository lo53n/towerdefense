﻿using UnityEngine;
using System.Collections;

public class ScreenMovement : MonoBehaviour {

    [System.Serializable]
    public struct MapCorners
    {
        public Transform MIN;
        public Transform MAX;
    }

    public MapCorners corners;
    public Camera cam;
    public float zoom;
    public float speed;
    public float mouseSpeed;

    //public float zoomSpeed = 1;
    //public float targetOrtho;
    //public float smoothSpeed = 2.0f;
    //public float minOrtho = 1.0f;
    //public float maxOrtho = 5.5f;

    //public Transform target;

    // Use this for initialization
    void Start ()
    {
        //targetOrtho = Camera.GetComponent<Camera>().orthographicSize;

        SetCornersOfMap();


    }
	
	// Update is called once per frame
	void Update ()
    {

        cam.orthographicSize = (Screen.height /100f) / zoom;

        //Vector3 movement;

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            this.transform.Translate(new Vector3(0, -speed * Time.deltaTime, 0));
        }
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            this.transform.Translate(new Vector3(0, speed * Time.deltaTime, 0));
        }



        if (Input.GetMouseButton(2))
        {

            transform.position -= new Vector3(Input.GetAxisRaw("Mouse X") * Time.deltaTime * mouseSpeed,
                                       Input.GetAxisRaw("Mouse Y") * Time.deltaTime * mouseSpeed, 0.0f);
        }


        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, corners.MIN.transform.position.x, corners.MAX.transform.position.x),
            Mathf.Clamp(transform.position.y, corners.MIN.transform.position.y, corners.MAX.transform.position.y),
            transform.position.z);




        //float scroll = Input.GetAxis("Mouse ScrollWheel");
        //if (scroll != 0.0f)
        //{
        //    targetOrtho -= scroll * zoomSpeed;
        //    targetOrtho = Mathf.Clamp(targetOrtho, minOrtho, maxOrtho);
        //}

        //Camera.GetComponent<Camera>().orthographicSize = Mathf.MoveTowards(Camera.GetComponent<Camera>().orthographicSize, targetOrtho, smoothSpeed * Time.deltaTime);
    }


    public void SetCornersOfMap()
    {
        GameObject extremes = GameObject.FindGameObjectWithTag("MapExtremes");
        
        corners.MIN = extremes.transform.GetChild(0);
        corners.MAX = extremes.transform.GetChild(1);
    }
}
