﻿using UnityEngine;
using System.Collections;

public class UIScripts : MonoBehaviour {





    void Start()
    {
       // GameObject.Find("UpgradeButton").SetActive(false);
    }
    #region Turret_building


    public void ToggleBuildingBasicTurret()
    {
        TurretBuilder tb = GameObject.FindObjectOfType<TurretBuilder>();

        tb.SelectBasicTurret();

    }

    public void ToggleBuildingSplashTurret()
    {
        TurretBuilder tb = GameObject.FindObjectOfType<TurretBuilder>();

        tb.SelectSplashTurret();
    }

    public void ToggleBuildingTremorTurret()
    {
        TurretBuilder tb = GameObject.FindObjectOfType<TurretBuilder>();

        tb.SelectTremorTurret();
    }

    #endregion


    public void UpgradeSelectedTurret()
    {
        GameObject.FindObjectOfType<GameManager>().UpgradeTurret();
    }

    public void SendWaveEarly()
    {
        GameObject.FindObjectOfType<EnemySpawner>().SendWaveEarlier();
    }


    #region Essence_upgrades

    public void IncreaseRedTier()
    {
        GameObject.FindObjectOfType<GameManager>().UpgradeTurretTier(EssenceColour.RED);
    }
    public void IncreaseGreenTier()
    {
        GameObject.FindObjectOfType<GameManager>().UpgradeTurretTier(EssenceColour.GREEN);
    }
    public void IncreaseBlueTier()
    {
        GameObject.FindObjectOfType<GameManager>().UpgradeTurretTier(EssenceColour.BLUE);
    }
    public void IncreaseCyanTier()
    {
        GameObject.FindObjectOfType<GameManager>().UpgradeTurretTier(EssenceColour.CYAN);
    }
    public void IncreaseYellowTier()
    {
        GameObject.FindObjectOfType<GameManager>().UpgradeTurretTier(EssenceColour.YELLOW);
    }
    public void IncreaseVioletTier()
    {
        GameObject.FindObjectOfType<GameManager>().UpgradeTurretTier(EssenceColour.VIOLET);
    }
    public void IncreaseGreyTier()
    {
        GameObject.FindObjectOfType<GameManager>().UpgradeTurretTier(EssenceColour.GREY);
    }
    public void IncreaseBlackTier()
    {
        GameObject.FindObjectOfType<GameManager>().UpgradeTurretTier(EssenceColour.BLACK);
    }
    
    #endregion

}
