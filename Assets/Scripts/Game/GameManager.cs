﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

    public Text materiaText;
    public Text redColourText;
    public Text greenColourText;
    public Text blueColourText;
    public Text cyanColourText;
    public Text yellowColourText;
    public Text violetColourText;
    public Text greyColourText;
    public Text blackColourText;
    public Text livesText;

    [System.Serializable]
    public struct Resources
    {
        public int materia;
        public int redEssence;
        public int greenEssence;
        public int blueEssence;
        public int cyanEssence;
        public int yellowEssence;
        public int violetEssence;
        public int greyEssence;
        public int blackEssence;

        public void IncreaseResource(int value, EssenceColour colour)
        {
            switch (colour)
            {
                case EssenceColour.NEUTRAL:
                    materia += value;
                    break;
                case EssenceColour.RED:
                    redEssence += value;
                    break;
                case EssenceColour.GREEN:
                    greenEssence += value;
                    break;
                case EssenceColour.BLUE:
                    blueEssence += value;
                    break;
                case EssenceColour.CYAN:
                    cyanEssence += value;
                    break;
                case EssenceColour.YELLOW:
                    yellowEssence += value;
                    break;
                case EssenceColour.VIOLET:
                    violetEssence += value;
                    break;
                case EssenceColour.GREY:
                    greyEssence += value;
                    break;
                case EssenceColour.BLACK:
                    blackEssence += value;
                    break;
            }

        }
        public void DecreaseResource(int value, EssenceColour colour)
        {
            switch (colour)
            {
                case EssenceColour.NEUTRAL:
                    materia -= value;
                    break;
                case EssenceColour.RED:
                    redEssence -= value;
                    break;
                case EssenceColour.GREEN:
                    greenEssence -= value;
                    break;
                case EssenceColour.BLUE:
                    blueEssence -= value;
                    break;
                case EssenceColour.CYAN:
                    cyanEssence -= value;
                    break;
                case EssenceColour.YELLOW:
                    yellowEssence -= value;
                    break;
                case EssenceColour.VIOLET:
                    violetEssence -= value;
                    break;
                case EssenceColour.GREY:
                    greyEssence -= value;
                    break;
                case EssenceColour.BLACK:
                    blackEssence -= value;
                    break;
            }

        }
    }

    public int maxLives;
    public int lives;


    public Resources res;

    EntitySelector entitySelector;

    // Use this for initialization
    void Start ()
    {
        entitySelector = gameObject.GetComponent<EntitySelector>();

        lives = maxLives;
        RefreshView();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void RefreshView()
    {
        materiaText.text = "Materia: " + res.materia;
        redColourText.text = "Red Essence: " + res.redEssence;
        greenColourText.text = "Green Essence: " + res.greenEssence;
        blueColourText.text = "Blue Essence: " + res.blueEssence;
        cyanColourText.text = "Cyan Essence: " + res.cyanEssence;
        yellowColourText.text = "Yellow Essence: " + res.yellowEssence;
        violetColourText.text = "Violet Essence: " + res.violetEssence;
        greyColourText.text = "Grey Essence: " + res.greyEssence;
        blackColourText.text = "Black Essence: " + res.blackEssence;
        livesText.text = "Lives left: " + lives + "/" + maxLives;


        entitySelector.RefreshStatsPanelInfo();

    }

    public void ResourceIncome(int value, EssenceColour colour)
    {
        res.IncreaseResource(value, colour);

        RefreshView();
    }
    public void ResourceExpense(int value, EssenceColour colour)
    {
        res.DecreaseResource(value, colour);

        RefreshView();
    }

    public int GetMateriaAmount()
    {
        return res.materia;
    }
    public Resources GetEssenceAmount()
    {
        return res;
    }

    public void UpgradeTurret()
    {

        if (entitySelector.selectedTurret.CanUpgradeTurret())
        {
            ResourceExpense(entitySelector.selectedTurret.upgradeInformations[entitySelector.selectedTurret.totalLevels.level].nextLevelCost, EssenceColour.NEUTRAL);
            entitySelector.UpgradeSelectedTurret();

            RefreshView();
        }
        
    }

    public void UpgradeTurretTier(EssenceColour colour)
    {

        if (entitySelector.selectedTurret.CanIncreaseTurretTier(colour))
        {
            ResourceExpense(entitySelector.selectedTurret.GetEssenceCosts(colour), colour);
            entitySelector.IncreaseSelectedTurretTier(colour);

            RefreshView();
        }
    }


    public void TakeLife(int amount)
    {
        lives -= amount;
        livesText.text = "Lives left: " + lives + "/" + maxLives;
    }

    
}
