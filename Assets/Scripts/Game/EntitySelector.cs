﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class EntitySelector : MonoBehaviour {


    [System.Serializable]
    public class TurretStatsTexts
    {
        public Text damage;
        public Text range;
        public Text ROF;
        public Text poison;
        public Text slow;
        public Text stun;
    }

    [System.Serializable]
    public class TurretUpgradesTexts
    {
        public Text name;
        public Text red;
        public Text green;
        public Text blue;
        public Text cyan;
        public Text yellow;
        public Text violet;
        public Text grey;
        public Text black;
    }

    [System.Serializable]
    public class TurretUpgradesButtons
    {
        public Button normal;
        public Button red;
        public Button green;
        public Button blue;
        public Button cyan;
        public Button yellow;
        public Button violet;
        public Button grey;
        public Button black;

    }
    [System.Serializable]
    public class TurretBuildButtons
    {
        public Button basicTurret;
        public Button tremorTurret;
        public Button splashTurret;
    }

    public GameObject statsPanels;
    public TurretStatsTexts statsTexts;
    public TurretUpgradesTexts upgradesTexts;
    public TurretUpgradesButtons upgradesButtons;
    public TurretBuildButtons buildButtons;


    public Turret selectedTurret;
    public Enemy selectedEnemy;



    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetMouseButtonDown(0))
        {
            EventSystem ES = GameObject.FindObjectOfType<EventSystem>();
            if (selectedTurret != null && !ES.IsPointerOverGameObject())
            {
                selectedTurret.SelectTurret(false);
                upgradesButtons.normal.gameObject.SetActive(false);
                statsPanels.SetActive(false);

                selectedTurret = null;
            }
        }
	}


    public void SelectTurret(Turret turret)
    {

        selectedTurret = turret;
        selectedTurret.SelectTurret(true);
        upgradesButtons.normal.gameObject.SetActive(true);
        statsPanels.SetActive(true);
        RefreshStatsPanelInfo();
    }

    public void SelectEnemy(Enemy enemy)
    {
        selectedTurret.SelectTurret(false);
        selectedTurret = null;
    }

    public void UpgradeSelectedTurret()
    {
        selectedTurret.UpgradeTurret();
    }

    public void IncreaseSelectedTurretTier(EssenceColour colour)
    {
        selectedTurret.IncreaseTurretTier(colour);
    }


    #region Refresh_UI

    public void RefreshStatsPanelInfo()
    {
        RefreshBuildTurretButtonStates();

        
        if (selectedTurret == null) return;
        
        RefreshTurretStats();
        RefreshTurretUpgrades();
        RefreshUpgradeButtonStates();

    }

    void RefreshTurretStats()
    {
        Turret.Stats ts = selectedTurret.GetTurretStats();

        statsTexts.damage.text = "Damage: " + ts.bulletDamage;
        statsTexts.range.text = "Range :" + ts.turretRange * 10;
        statsTexts.ROF.text = "ROF: " + ts.fireCooldown; ;
        statsTexts.poison.text = "Poison : " + ts.poisonDamage;
        statsTexts.slow.text = "Slow: " + ts.slow + "% for " + ts.slowTime + "s";
        statsTexts.stun.text = "Stun chance: " + ts.stun + "% for " + ts.stunTime + "s";
    }

    void RefreshTurretUpgrades()
    {
        Turret.Upgrades tu = selectedTurret.GetTotalTurretUpgrades();

        upgradesTexts.name.text = selectedTurret.GetTurretName();
        upgradesTexts.red.text = "Red Level: " + tu.redTier;
        upgradesTexts.green.text = "Green Level: " + tu.greenTier;
        upgradesTexts.blue.text = "Blue Level: " + tu.blueTier;
        upgradesTexts.cyan.text = "Cyan Level: " + tu.cyanTier;
        upgradesTexts.yellow.text = "Yellow Level: " + tu.yellowTier;
        upgradesTexts.violet.text = "Violet Level: " + tu.violetTier;
        upgradesTexts.grey.text = "Grey Level: " + tu.greyTier;
        upgradesTexts.black.text = "Black Level: " + tu.blackTier;

        //Turret.ResValues uc = selectedTurret.GetEssenceCosts();

        upgradesButtons.red.GetComponentsInChildren<Text>()[0].text = selectedTurret.GetEssenceCosts(EssenceColour.RED).ToString();
        upgradesButtons.green.GetComponentsInChildren<Text>()[0].text = selectedTurret.GetEssenceCosts(EssenceColour.GREEN).ToString();
        upgradesButtons.blue.GetComponentsInChildren<Text>()[0].text = selectedTurret.GetEssenceCosts(EssenceColour.BLUE).ToString();
        upgradesButtons.cyan.GetComponentsInChildren<Text>()[0].text = selectedTurret.GetEssenceCosts(EssenceColour.CYAN).ToString();
        upgradesButtons.yellow.GetComponentsInChildren<Text>()[0].text = selectedTurret.GetEssenceCosts(EssenceColour.YELLOW).ToString();
        upgradesButtons.violet.GetComponentsInChildren<Text>()[0].text = selectedTurret.GetEssenceCosts(EssenceColour.VIOLET).ToString();
        upgradesButtons.grey.GetComponentsInChildren<Text>()[0].text = selectedTurret.GetEssenceCosts(EssenceColour.GREY).ToString();
        upgradesButtons.black.GetComponentsInChildren<Text>()[0].text = selectedTurret.GetEssenceCosts(EssenceColour.BLACK).ToString();
    }

    void RefreshUpgradeButtonStates()
    {
        if (selectedTurret.CanUpgradeTurret()) upgradesButtons.normal.interactable = true;
        else upgradesButtons.normal.interactable = false;

        if (selectedTurret.CanIncreaseTurretTier(EssenceColour.RED)) upgradesButtons.red.interactable = true;
        else upgradesButtons.red.interactable = false;
        if (selectedTurret.CanIncreaseTurretTier(EssenceColour.GREEN)) upgradesButtons.green.interactable = true;
        else upgradesButtons.green.interactable = false;
        if (selectedTurret.CanIncreaseTurretTier(EssenceColour.BLUE)) upgradesButtons.blue.interactable = true;
        else upgradesButtons.blue.interactable = false;
        if (selectedTurret.CanIncreaseTurretTier(EssenceColour.CYAN)) upgradesButtons.cyan.interactable = true;
        else upgradesButtons.cyan.interactable = false;
        if (selectedTurret.CanIncreaseTurretTier(EssenceColour.YELLOW)) upgradesButtons.yellow.interactable = true;
        else upgradesButtons.yellow.interactable = false;
        if (selectedTurret.CanIncreaseTurretTier(EssenceColour.VIOLET)) upgradesButtons.violet.interactable = true;
        else upgradesButtons.violet.interactable = false;
        if (selectedTurret.CanIncreaseTurretTier(EssenceColour.GREY)) upgradesButtons.grey.interactable = true;
        else upgradesButtons.grey.interactable = false;
        if (selectedTurret.CanIncreaseTurretTier(EssenceColour.BLACK)) upgradesButtons.black.interactable = true;
        else upgradesButtons.black.interactable = false;
        
    }

    void RefreshBuildTurretButtonStates()
    {
        TurretBuilder tb = GameObject.FindObjectOfType<TurretBuilder>();


        if (tb.CanBuildBasicTurret()) buildButtons.basicTurret.interactable = true;
        else buildButtons.basicTurret.interactable = false;

        if (tb.CanBuildTremorTurret()) buildButtons.tremorTurret.interactable = true;
        else buildButtons.tremorTurret.interactable = false;

        if (tb.CanBuildSplashTurret()) buildButtons.splashTurret.interactable = true;
        else buildButtons.splashTurret.interactable = false;

    }
    #endregion
}
