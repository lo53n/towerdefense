﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;


public class TurretBuilder : MonoBehaviour {
        
    [HideInInspector]
    public bool isBuildingTime;
    [HideInInspector]
    public bool canBuildInThatPlace;

    [System.Serializable]
    public class TurretBuildingData
    {
        public GameObject turretPrefab;
        public GameObject turretPlacementPrefab;
        public int initialCost;
    }


    public TurretBuildingData basicTurretData;
    public TurretBuildingData tremorTurretData;
    public TurretBuildingData splashTurretData;

    TurretBuildingData selectedPrefabData;
    
    GameObject tPlacement;

    GameManager gm;


    // Use this for initialization
    void Start () {
        gm = GameObject.FindObjectOfType<GameManager>();

    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonUp(1) && isBuildingTime)
        {
            ToggleBuilding();
        }
        if (Input.GetMouseButtonUp(0))
        {
            EventSystem ES = GameObject.FindObjectOfType<EventSystem>();
            if (isBuildingTime && !ES.IsPointerOverGameObject() && canBuildInThatPlace)
            {
                GameObject turretGO = (GameObject)Instantiate(selectedPrefabData.turretPrefab, tPlacement.transform.position, tPlacement.transform.rotation);
                Turret tur = turretGO.GetComponent<Turret>();
                gm.ResourceExpense(selectedPrefabData.initialCost, EssenceColour.NEUTRAL);
                tur.IncreaseTurretValue(selectedPrefabData.initialCost);
                tPlacement.transform.rotation = Quaternion.Euler(0, 0, 0);
                ToggleBuilding();
            }
        }

        if (isBuildingTime && tPlacement != null)
        {
            if (Input.GetMouseButton(0))
            {
                Vector3 inputPos = Input.mousePosition;
                Vector3 targetPos = Camera.main.ScreenToWorldPoint(new Vector3(inputPos.x, inputPos.y, 10f));

                Vector3 vectorToTarget = targetPos - tPlacement.transform.position;
                float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
                Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);
                tPlacement.transform.rotation = Quaternion.Euler(0, 0, q.eulerAngles.z);
            }
            else
            {
                Vector3 inputPos = Input.mousePosition;
                Vector3 targetPos = Camera.main.ScreenToWorldPoint(new Vector3(inputPos.x, inputPos.y, 10f));
                tPlacement.transform.position = targetPos;
                tPlacement.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }
    }

    #region Turret_Selection

    public bool CanBuildBasicTurret()
    {

        if (gm.res.materia >= basicTurretData.initialCost)
            return true;

        return false;
    }
    public void SelectBasicTurret()
    {
        if(isBuildingTime && selectedPrefabData == basicTurretData)
        {
            ToggleBuilding();
        }
        
        selectedPrefabData = basicTurretData;
        if (!isBuildingTime) ToggleBuilding();
    }


    public bool CanBuildTremorTurret()
    {

        if (gm.res.materia >= tremorTurretData.initialCost)
            return true;

        return false;
    }
    public void SelectTremorTurret()
    {
        if (isBuildingTime && selectedPrefabData == tremorTurretData)
        {
            ToggleBuilding();
        }
        
        selectedPrefabData = tremorTurretData;
        if (!isBuildingTime) ToggleBuilding();
    }


    public bool CanBuildSplashTurret()
    {

        if (gm.res.materia >= splashTurretData.initialCost)
            return true;

        return false;
    }
    public void SelectSplashTurret()
    {
        if (isBuildingTime && selectedPrefabData == splashTurretData)
        {
            ToggleBuilding();
        }
        
        selectedPrefabData = splashTurretData;
        if (!isBuildingTime) ToggleBuilding();
    }

    #endregion

    public void ToggleBuilding()
    {
        if (isBuildingTime)
        {
            isBuildingTime = false;
            Destroy(tPlacement);
        }
        else
        {
            isBuildingTime = true;
            Vector3 inputPos = Input.mousePosition;
            Vector3 targetPos = Camera.main.ScreenToWorldPoint(new Vector3(inputPos.x, inputPos.y, 10f));
            GameObject TP = (GameObject)Instantiate(selectedPrefabData.turretPlacementPrefab, targetPos, selectedPrefabData.turretPlacementPrefab.transform.rotation);
            tPlacement = TP;
        }
    }

    public bool isBuilding()
    {
        return isBuildingTime;
    }

    public void setBuildingPermit(bool permit)
    {
        canBuildInThatPlace = permit;
    }
}
