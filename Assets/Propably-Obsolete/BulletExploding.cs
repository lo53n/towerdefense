﻿using UnityEngine;
using System.Collections;

public class BulletExploding : Bullet
{
    public override void BulletHit()
    {

        if (target != null)
        {
            target.gameObject.GetComponent<Enemy>().TakeDamage(stats.bulletDamage);
        }
        Collider[] colls = Physics.OverlapSphere(transform.position, stats.splashRadius);
        foreach(Collider c in colls)
        {
            Enemy e = c.GetComponent<Enemy>();
            if(e != null)
            {
                e.TakeSplashDamage(stats.splashDamage);
            }
        }
        Destroy(gameObject);
    }

    void Start()
    {
        isTargetAlive = false;
        lastKnownPosition = target.position;
        target = null;
    }
}
