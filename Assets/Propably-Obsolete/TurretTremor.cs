﻿using UnityEngine;
using System.Collections;

public class TurretTremor : Turret {


    public override void RotateCannonAtTarget()
    {

    }

    public override void ShootAt()
    {
        Collider[] colls = Physics.OverlapSphere(transform.position, stats.turretRange);
        foreach (Collider c in colls)
        {
            Enemy e = c.GetComponent<Enemy>();
            if (e != null)
            {
                e.TakeSplashDamage(stats.tremorDamage);
                e.TakePoisonDamage(stats.poisonDamage);
            }
        }
    }

    public override void IncreaseTurretTierStats(EssenceColour colour)
    {
        switch (colour)
        {
            case EssenceColour.RED:
                stats.tremorDamage *= 2f;
                break;

            default: return;
        }
    }

}
