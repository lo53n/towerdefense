﻿using UnityEngine;
using System.Collections;

public class BulletRicocheting : Bullet {
    

    public override void BulletHit()
    {
        stats.ricochets--;
        if (target != null)
        {
            target.gameObject.GetComponent<Enemy>().TakeDamage(stats.bulletDamage);
        }
        if(stats.ricochets < 0)
        {
            Destroy(gameObject);
        }
        else
        {
            LookForNewTarget();
        }
    }


    void LookForNewTarget()
    {
        Collider[] colls = Physics.OverlapSphere(transform.position, 1);
        if (colls.Length > 0)
        {
            float dist = Mathf.Infinity;
            foreach (Collider c in colls)
            {
                Enemy e = c.GetComponent<Enemy>();
                if (e != null)
                {

                    if (target != null && target.transform == e.transform) continue;


                    float d = Vector3.Distance(transform.position, e.transform.position);
                    if (d < dist)
                    {
                        dist = d;

                        target = e.transform;
                    }

                }
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }

    /*
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    */
}
